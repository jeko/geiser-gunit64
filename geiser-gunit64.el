;;; geiser-gunit64.el --- Gunit64 tools for Geiser. -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Jérémy Korwin-Zmijowski

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;; See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;; Author: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Maintainer: Jérémy Korwin-Zmijowski <jeremy@korwin-zmijowski.fr>
;; Keywords: scheme srfi64 tools testing
;; Version: 0.0.1
;; Homepage: https://framagit.org/jeko/geiser-gunit64
;; Package-Requires: ((emacs "25.1") (geiser "0.27"))

;;; Commentary:

;; geiser-gunit64.el is a minor mode to provide facilities to manage tests
;; in Emacs.

;;; Code:

(require 'geiser-mode)

(defun run-tests ()
  "Execute the gunit64 root test suite."
  (interactive)
  (save-some-buffers t)
  (geiser-compile-current-buffer)
  (with-temp-buffer
    (geiser-mode)
    (insert "(run-root-tests)")
    (geiser-eval-last-sexp t)))

(defun string-at-point ()
  "Grab the string at point, including the double straight quotes."
  (interactive)
  (let (p1 p2)
    (skip-chars-backward "^\"")
    (setq p1 (point))
    (skip-chars-forward "^\"")
    (setq p2 (point))
    (buffer-substring-no-properties (- p1 1) (+ p2 1))))

(defun run-test-at-point ()
  "Execute the test at point."
  (interactive)
  (save-some-buffers t)
  (geiser-compile-current-buffer)
  (let ((test-to-run (string-at-point)))
    (with-temp-buffer
      (geiser-mode)
      (insert (format "(run-only-tests '(%s))" test-to-run))
      (geiser-eval-last-sexp t))))

(defun re-run-tests ()
  "Re-run the last test execution."
  (interactive)
  (save-some-buffers t)
  (geiser-compile-current-buffer)
  (with-temp-buffer
    (geiser-mode)
    (insert "(re-run-tests)")
    (geiser-eval-last-sexp t)))

(defun run-failed-tests ()
  "Re-run the last test execution's failed test."
  (interactive)
  (save-some-buffers t)
  (geiser-compile-current-buffer)
  (with-temp-buffer
    (geiser-mode)
    (insert "(run-failed-tests)")
    (geiser-eval-last-sexp t)))

(define-minor-mode geiser-gunit64-mode
  "Spell out tests for your Guile app."
  :keymap (let ((map (make-sparse-keymap)))
	    (define-key map (kbd "C-c r t") 'run-tests)
	    (define-key map (kbd "C-c r .") 'run-test-at-point)
	    (define-key map (kbd "C-c r r") 're-run-tests)
	    (define-key map (kbd "C-c r f") 'run-failed-tests)
            map))

(provide 'geiser-gunit64)
;;; geiser-gunit64.el ends here
